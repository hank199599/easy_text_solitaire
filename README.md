說明 檔案分支樹   
-------
functions  
|   
|- cjst.json---------------拼音對應的台灣注音對照表  
|- index.js----------------Action的後臺原始碼  
|- package.json------------Action的後臺模組  
|- text_library.json-------以字首讀音分類，所有詞彙的陣列表  
|- zhuyin.json-------------每一個詞對應的注音表  